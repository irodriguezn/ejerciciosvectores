package vector;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nachorod
 */
public class Vector {
    private int v[];
    
    public Vector(int num) {
        v=new int[num];
    }
    
    public int[] getVector() {
        return v;
    }
    
    public void mostrar() {
        for (int el: v) {
            System.out.print(el + " ");
        }
        System.out.println("");
        System.out.println("");
    }
    
    public void rellenar(int min, int max) {
        for (int i=0; i<v.length; i++) {
            v[i]=(int)(Math.random()*max+min);
        }
    }
    
    public void insertar(int pos, int num) {
        v[pos]=num;
    }
    
    public void insertarLinea(String numeros) {
        int num=0;
        for (int i=0; i<v.length; i++) {
            num=obtenerSiguiente(numeros);
            numeros=extraerNumero(numeros);
            v[i]=num;
        }
    }
    
    public void mostrarPosPar() {
        for (int i=1; i<v.length; i+=2) {
            System.out.print(v[i]+ " ");
        }
        System.out.println("");
    }

    public void mostrarPosImpar() {
        for (int i=0; i<v.length; i+=2) {
            System.out.print(v[i]+ " ");
        }
        System.out.println("");
    }    
    
    public void mostrarCuatro() {
        String aux="";
        int cont=1;
        for (int i=0; i<v.length;i++) {
            if (cont<=4) {
               aux+=v[i]+" ";
               cont++;
            } else {
                System.out.println(aux);
                cont=1;
                aux=v[i]+" ";
                cont++;
            }
        }
        System.out.println(aux);
        System.out.println("");
    }
    
    public void mostrarMult4() {
        String aux="";
        for (int i=3;i<v.length;i+=4) {
            aux+=v[i]+" ";
        }
        System.out.println(aux);
        System.out.println("");
    }
    
    public void mostrarMult4b() {
        String aux="";
        for (int i=0;i<v.length;i++) {
            if ((i+1)%4==0) {
                aux+=v[i]+" ";
            }
        }
        System.out.println(aux);
        System.out.println("");
    }
    
    public void mostrarParYPos(boolean mostrarPares) {
        // 1 2 3 4 5 6
        // Num 2 -> Pos 2 
        // Num 4 -> Pos 4
        for (int i=0; i<v.length;i++) {
            if (mostrarPares) {
                if (v[i]%2==0) {
                    System.out.println("Num " + v[i] + " -> Pos " + (i+1));
                }
            } else {
                if (v[i]%2!=0) {
                    System.out.println("Num " + v[i] + " -> Pos " + (i+1));
                }
            }
        }
        System.out.println("");
    }
    
public void mostrarContenidoParPosPar() {
        for (int i=1; i<v.length;i+=2) {
            if (v[i]%2==0) {
                System.out.println("Num " + v[i] + " -> Pos " + (i+1));
            }
        }
        System.out.println("");
    }    
    
    public void mostrarContenidox3PosImPar() {
        for (int i=0; i<v.length;i+=2) {
            if (v[i]%3==0 && v[i]>=3) {
                System.out.println("Num " + v[i] + " -> Pos " + (i+1));
            }
        }
        System.out.println("");
    } 

    public void sumarContenidoPar () {
        // 1 2 3 4
        int suma=0;
        for (int elemento:v) {
            if (elemento%2==0) {
                suma+=elemento;
            }
        }
        System.out.println("La suma de los elementos pares es " + suma);
    }
    
    public boolean hayRepetidos() {
        boolean hay=false;
        int clave;
        for (int i=0;i<v.length;i++) {
            for (int j=i+1; j<v.length;j++) {
                if (v[i]==v[j]) {
                    hay=true;
                    break;
                }
            }
            if (hay) {
                break;
            }
        }
        return hay;
    }
    
    public void burbuja() {
        int temp;
        for (int i=0; i<v.length; i++) {
            for (int j=v.length-2; j>=i;j--) {
                if (v[j+1]<v[j]) {
                    temp=v[j+1];
                    v[j+1]=v[j];
                    v[j]=temp;
                }
            }
        }
    }
    
    public void sumarAnteriores(Vector w) {
        int suma=0;
        int u[];
        u=w.getVector();
        for (int i=0; i<u.length;i++) {
            for (int j=0; j<=i; j++) {
                suma+=u[j];
            }
            v[i]=suma; // v[0]=1
            suma=0;
        }
    }
    
    static int obtenerSiguiente(String numeros) {
        int num=0;
        int pos=0;
        String numero;
        numeros=numeros+" ";
        pos=numeros.indexOf(" ");
        numero=numeros.substring(0, pos);
        num=Integer.parseInt(numero);
        return num;
    }
    static String extraerNumero(String numeros) {
        int pos=0;
        String aux;
        pos=numeros.indexOf(" ");
        aux=numeros.substring(pos+1, numeros.length());
        return aux;
    }
}
